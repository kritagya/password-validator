import re


def check_min_length(password):
	""" checks if the length of password is at least 6 or not """
	if len(password) >= 6:
		return True
	else:
		print ("Failure Password must be at least 6 characters long.")
		return False


def check_max_length(password):
	""" checks if the length of password is at max 12 or not """
	if len(password) <= 12:
		return True
	else:
		print ("Failure Password must be at max 12 characters long.")
		return False


def check_letters_not_allowed(password):
	""" checks if the password contains any character that is not allowed """
	if not re.search("[%!)(]",password):
		return True
	else:
		print ("Failure Password cannot contain %!)( .")
		return False


def check_uppercase_letter(password):
	""" checks if the password has atleast 1 capital alphabet """
	if re.search("[A-Z]",password):
		return True
	else:
		print ("Failure Password must contain at least one letter from A-Z.")
		return False


def check_lowercase_letter(password):
	""" checks if the password has atleast 1 small alphabet """
	if re.search("[a-z]",password):
		return True
	else:
		print ("Failure Password must contain at least one letter from a-z.")
		return False


def check_digits(password):
	""" checks if the password has atleast 1 digit """
	if re.search("[0-9]",password):
		return True
	else:
		print ("Failure Password must contain at least one letter from 0-9.")
		return False


def check_special_char(password):
	""" checks if the password has atleast 1 special character from [*$_#=@] """
	if re.search("[*$_#=@]",password):
		return True
	else:
		print ("Failure Password must contain at least one letter from *$_#=@.")
		return False


def check_validity(password):
	""" checks if the password is valid"""
	print (password,end=" ")
	if (check_min_length(password) and check_max_length(password) and check_lowercase_letter(password) \
		and check_digits(password) and check_uppercase_letter(password) and check_special_char(password)) \
		and check_letters_not_allowed(password) :

		print ("Success")


def parse_passwords(input_passwords):
	""" parse passwords from input string """
	passwords = input_passwords.split(",")
	return passwords


if __name__ == "__main__":

	input_passwords = input()

	passwords = parse_passwords(input_passwords)
	for password in passwords:
		check_validity(password)


