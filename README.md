# Password-Validator

This is a password validator program, written in python 3.6.</br>
This program checks the validity of password input by users with respect to following rules:</br>
1. Minimum length: 6</br>
2. Maximum length: 12</br>
3. At least 1 letter in [a-z], [0-9], [A-Z] and [*$_#=@] each.</br>
4. It should not contain any letter from [%!)(]</br>

Instructions </br>

1.) Clone the files in the root folder.</br>

2.) Use python 3.6 to run the program.</br>

3.) Run program</br>
> python password_validator.py

4.) Input a sequence of comma separated passwords.</br>